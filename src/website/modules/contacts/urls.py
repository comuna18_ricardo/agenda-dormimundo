from django.views.generic import TemplateView

from django.contrib.auth.decorators import login_required
from django.urls import path, include, re_path

from website.modules.contacts import views

app_name = "contacts"


urlpatterns = [
    re_path(r'^$', views.Index.as_view(), name='index'),
    re_path(r'^nuevo/$', views.Create.as_view(), name='create'),
    re_path(r'^(?P<pk>[-\w]+)/$', views.Detail.as_view(), name='detail'),
    re_path(r'^(?P<pk>[-\w]+)/actualizar/$', views.Update.as_view(), name='update'),
    re_path(r'^(?P<pk>[-\w]+)/borrar/$', views.Delete.as_view(), name='delete'),
    re_path(r'^(?P<pk>[-\w]+)/telefonos/',  include('website.modules.contacts.phones.urls', namespace='phones')),
    re_path(r'^(?P<pk>[-\w]+)/direcciones/',  include('website.modules.contacts.addresses.urls', namespace='addresses')),
]

from django.urls import reverse

from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView, DetailView

from contacts.models import Contact
from contacts.models import SEX_M, SEX_F

from django.contrib.auth.mixins import LoginRequiredMixin


class Index(LoginRequiredMixin, ListView):
    template_name = 'website/contacts/index.html'
    model = Contact

    def get_queryset(self):
        webuser = self.request.user.webuser
        return Contact.objects.filter(webuser=webuser)


class Create(LoginRequiredMixin, CreateView):
    template_name = 'website/contacts/create.html'
    model = Contact
    fields = ['first_name', 'last_name', 'sex']

    def post(self, request, *args, **kwargs):
        self.object = None
        form = self.get_form()
        if form.is_valid():
            cd = form.cleaned_data
            if Contact.objects.filter(first_name=cd['first_name'], last_name=cd['last_name'], sex=cd['sex']).count() > 0:
                form.add_error('first_name', 'Ya esta registrado este nombre')
                return self.form_invalid(form)

            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        webuser = self.request.user.webuser
        form.instance.webuser = webuser

        return super().form_valid(form)

    def get_success_url(self):
        return reverse('website:contacts:detail', kwargs={
            'pk': self.object.pk
        })


class Update(LoginRequiredMixin, UpdateView):
    template_name = 'website/contacts/update.html'
    model = Contact
    fields = ['first_name', 'last_name', 'sex']

    def get_success_url(self):
        return reverse('website:contacts:index')


class Delete(LoginRequiredMixin, DeleteView):
    template_name = 'website/contacts/delete.html'
    model = Contact

    def get_success_url(self):
        return reverse('website:contacts:index')


class Detail(LoginRequiredMixin, DetailView):
    template_name = 'website/contacts/detail.html'
    model = Contact

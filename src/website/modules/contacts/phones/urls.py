from django.views.generic import TemplateView

from django.contrib.auth.decorators import login_required
from django.urls import path, include, re_path

from website.modules.contacts.phones import views

app_name = "phones"


urlpatterns = [
    re_path(r'^nuevo/$', views.Create.as_view(), name='create'),
    re_path(r'^(?P<phone_pk>[-\w]+)/actualizar/$', views.Update.as_view(), name='update'),
    re_path(r'^(?P<phone_pk>[-\w]+)/borrar/$', views.Delete.as_view(), name='delete'),
]

from django.urls import reverse

from django.views.generic import TemplateView, ListView, CreateView, UpdateView, DeleteView, DetailView

from contacts.models import Contact, Phone, Address

from django.contrib.auth.mixins import LoginRequiredMixin


class Create(LoginRequiredMixin, CreateView):
    template_name = 'website/contacts/addresses/create.html'
    model = Address
    fields = ['address', 'type',]

    def get_contact(self):
        print(self.kwargs)
        return Contact.objects.get(pk=self.kwargs['pk'])

    def form_valid(self, form):
        contact = self.get_contact()

        form.instance.contact = contact

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = self.get_contact()

        context.update({
            'contact': contact,
        })

        return context

    def get_success_url(self):
        contact = self.get_contact()
        return reverse('website:contacts:detail', kwargs={
            'pk': contact.pk
        })


class Update(LoginRequiredMixin, UpdateView):
    template_name = 'website/contacts/addresses/update.html'
    model = Address
    fields = ['address', 'type', ]
    pk_url_kwarg = 'address_pk'

    def get_contact(self):
        return Contact.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = self.get_contact()

        context.update({
            'contact': contact,
        })

        return context

    def get_success_url(self):
        contact = self.get_contact()

        return reverse('website:contacts:detail', kwargs={
            'pk': contact.pk
        })


class Delete(LoginRequiredMixin, DeleteView):
    template_name = 'website/contacts/addresses/delete.html'
    model = Address
    pk_url_kwarg = 'address_pk'

    def get_contact(self):
        return Contact.objects.get(pk=self.kwargs['pk'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        contact = self.get_contact()

        context.update({
            'contact': contact,
        })

        return context

    def get_success_url(self):
        contact = self.get_contact()

        return reverse('website:contacts:detail', kwargs={
            'pk': contact.pk
        })


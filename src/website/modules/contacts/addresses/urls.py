from django.views.generic import TemplateView

from django.contrib.auth.decorators import login_required
from django.urls import path, include, re_path

from website.modules.contacts.addresses import views

app_name = "addresses"


urlpatterns = [
    re_path(r'^nuevo/$', views.Create.as_view(), name='create'),
    re_path(r'^(?P<address_pk>[-\w]+)/actualizar/$', views.Update.as_view(), name='update'),
    re_path(r'^(?P<address_pk>[-\w]+)/borrar/$', views.Delete.as_view(), name='delete'),
]

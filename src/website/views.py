import warnings
from django.contrib import messages

from django.shortcuts import render
from django.views.generic import TemplateView, RedirectView, FormView
from django.contrib.sites.shortcuts import get_current_site

from django.contrib.auth.views import LoginView

from django.contrib.auth.mixins import LoginRequiredMixin

from django.contrib.auth.models import User

from .forms import EmailAuthenticationForm

from profiles.services import create_webuser
from django import forms



class Index(LoginRequiredMixin, TemplateView):
    template_name = 'website/index.html'


####################
# Django Auth and Login Views
####################

# login
class EmailLoginView(LoginView):
    form_class = EmailAuthenticationForm
    success_url = '/'


def login(request, *args, **kwargs):
    return EmailLoginView.as_view(**kwargs)(request, *args, **kwargs)


class SignUpForm(forms.Form):
    first_name = forms.CharField(label='Nombres')
    last_name = forms.CharField(label='Apellidos')
    email = forms.EmailField(label='Email')
    password_1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password_2 = forms.CharField(label='Confirme Contraseña', widget=forms.PasswordInput)

    def clean(self):
        if self.cleaned_data['password_1'] != self.cleaned_data['password_2']:
            self.add_error('password_2', 'Las contraseñas no coinciden')
            self.add_error('password_1', 'Las contraseñas no coinciden')
            raise forms.ValidationError("Las contraseñas no coinciden")

        return self.cleaned_data


class signup(FormView):
    form_class = SignUpForm
    template_name = 'registration/signup.html'
    user = None

    def form_valid(self, form):
        data = form.cleaned_data
        current_site = get_current_site(self.request)
        domain = current_site.domain


        user = create_webuser(**{
            'first_name': data['first_name'],
            'last_name': data['last_name'],
            'email': data['email'],
            'password': data['password_1'],
            'domain': domain
        })

        self.user = user

        return super().form_valid(form)

    def get_success_url(self):
        login(self.request, self.user)
        messages.info(self.request, 'Cuenta creada correctamente. Inicie SesiÃ³n para continuar.')
        return '/login/'

    def post(self, request, *args, **kwargs):
        self.object = None

        form = self.get_form()
        if form.is_valid():

            email = form.cleaned_data['email']

            if len(User.objects.filter(email=email))>0:
                form.add_error('email', 'Este email ya estÃ¡ registrado.')
                return self.form_invalid(form)

            return self.form_valid(form)
        else:
            return self.form_invalid(form)


from django.contrib.auth.decorators import login_required
from django.urls import path, include, re_path

from django.contrib.auth import views as auth_views
from website.views import login, signup

from website import views

app_name = "website"


urlpatterns = [
    re_path(r'^$', views.Index.as_view(), name='index'),

    re_path(r'^login/$', login, name="login"),
    re_path(r'^registro/$', signup.as_view(), name="signup"),
    re_path(r'^logout/$', auth_views.logout_then_login, name="logout"),

    re_path(r'^contactos/', include('website.modules.contacts.urls', namespace='contacts')),
]

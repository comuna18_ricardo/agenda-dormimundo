# Generated by Django 2.1.5 on 2019-01-29 14:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='webuser',
            name='phone',
            field=models.CharField(default='', max_length=64, verbose_name='Teléfono'),
            preserve_default=False,
        ),
    ]

from django.db import models

from django.contrib.auth.models import User

#models.CASCADE
#models.SET_NULL
#models.PROTECT

class WebUser(models.Model):
    class Meta:
        verbose_name = 'Usuario Web'
        verbose_name_plural = 'Usuarios Web'

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    user = models.OneToOneField(User, on_delete=models.CASCADE)

    first_name = models.CharField('Nombre', max_length=128)
    last_name = models.CharField('Apellidos', max_length=128, blank=True, null=True)

    phone = models.CharField('Teléfono', max_length=64)

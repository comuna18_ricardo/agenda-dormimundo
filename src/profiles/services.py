from django.contrib.auth.models import User, Group
from profiles.models import WebUser

from app.services import create_random_string



def create_webuser(first_name, last_name, email, password=None, domain=None):
    # Todo: verify email availability

    username = create_random_string()
    user = User.objects.create_user(username, email, password)
    user.is_active = True
    user.save()


    # create donor
    wuser = WebUser(
        first_name=first_name,
        last_name=last_name,
        user=user
    )

    wuser.save()

    # send_activation_email(donor, domain)

    return wuser


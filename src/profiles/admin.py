from django.contrib import admin

from .models import WebUser


@admin.register(WebUser)
class WebUserAdmin(admin.ModelAdmin):
    pass

from django.contrib import admin

from .models import Contact, Address, Phone


class AddressInline(admin.TabularInline):
    model = Address
    max_num = 5
    min_num = 1



class PhoneInline(admin.TabularInline):
    model = Phone
    max_num = 2
    min_num = 1



@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display = ['first_name', 'last_name', 'sex']
    list_filter = ['sex',]
    search_fields = ['first_name', 'last_name']
    inlines = [
        AddressInline,
        PhoneInline,
    ]


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    pass


@admin.register(Phone)
class PhoneAdmin(admin.ModelAdmin):
    pass

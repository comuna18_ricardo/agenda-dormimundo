from django.db import models

SEX_M = 'M'  # MASCULINO
SEX_F = 'F'  # FEMENINO

SEX_CHOICES = (
    (SEX_F, 'Mujer'),
    (SEX_M, 'Hombre'),
)

TYPE_HOME = 'HOME'  # MASCULINO
TYPE_OFFICE = 'OFFICE'  # FEMENINO

TYPE_CHOICES = (
    (TYPE_HOME, 'Casa'),
    (TYPE_OFFICE, 'Oficina'),
)


class Contact(models.Model):
    class Meta:
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    webuser = models.ForeignKey('profiles.WebUser', on_delete=models.CASCADE, blank=True, null=True)

    first_name = models.CharField('Nombre', max_length=128)
    last_name = models.CharField('Apellidos', max_length=128, blank=True, null=True)
    sex = models.CharField('Sexo', max_length=8, default=SEX_F, choices=SEX_CHOICES)


class Phone(models.Model):
    class Meta:
        verbose_name = 'Teléfono'
        verbose_name_plural = 'Telefonos'

    def __str__(self):
        return "{}".format(self.phone)

    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, verbose_name="Contacto")

    phone = models.CharField('Teléfono', max_length=128)
    type = models.CharField('Tipo', max_length=8, default=TYPE_OFFICE, choices=TYPE_CHOICES)


class Address(models.Model):
    class Meta:
        verbose_name = 'Dirección'
        verbose_name_plural = 'Direcciones'

    def __str__(self):
        return "{}".format(self.address)

    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, verbose_name="Contacto")

    address = models.CharField('Dirección', max_length=512)
    type = models.CharField('Tipo', max_length=8, default=TYPE_OFFICE, choices=TYPE_CHOICES)

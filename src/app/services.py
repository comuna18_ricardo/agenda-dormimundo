import uuid


def create_random_string():
    return str(uuid.uuid4())[:30]


def create_credentials():
    username = create_random_string()
    password = create_random_string()

    return {
        'username': username,
        'password': password
    }
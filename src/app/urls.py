"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path

from django.contrib.auth import views as auth_views

from rest_framework import routers
from app import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)

router.register(r'contacts', views.ContactViewSet)
router.register(r'phones', views.PhoneViewSet)
router.register(r'addresses', views.AddressViewSet)
router.register(r'webusers', views.WebUsersViewSet)

urlpatterns = [
    re_path( r'^', include('website.urls', namespace='website')),
    re_path(r'^password_reset/$',
            auth_views.PasswordResetView.as_view(template_name='registration/password_reset.html'),
            name='password_reset'),
    re_path(r'^password_reset/done/$',
            auth_views.PasswordChangeDoneView.as_view(template_name='registration/password_reset_sent.html'),
            name='password_reset_done'),
    re_path(r'^reset/done/$',
            auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_success.html'),
            name='password_reset_complete'),
    re_path(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_change.html'),
            name='password_reset_confirm'),
    re_path(r'^api/', include(router.urls)),
    re_path(r'^api-auth/', include('rest_framework.urls')),
    path('admin/', admin.site.urls),
]

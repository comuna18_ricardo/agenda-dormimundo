from django.contrib.auth.models import User, Group
from rest_framework import serializers

from contacts.models import Contact, Phone, Address
from profiles.models import WebUser


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class ContactSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Contact
        fields = ('url', 'first_name', 'last_name', 'sex', 'webuser')


class WebUserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = WebUser
        fields = ('url', 'first_name', 'last_name', 'phone', )


class PhoneSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Phone
        fields = ('url', 'contact', 'phone', 'type')

class AddressSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Address
        fields = ('url', 'contact', 'address')

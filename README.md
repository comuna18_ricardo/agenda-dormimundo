# Agenda

Proyecto de prueba para Dormimundo.

  - Python 3
  - Django 2
  - SQLite

### Version
0.0.1

### Instalación

Consideraciones y especificaciones de instalación del proyecto.

Creación de ambiente virtual
```sh
$ virtualenv -p python3 venv_folder  # Usando Virtualenv
$ python3 -m 'venv' venv_folder  # Usando módulo de Python3
$ source venv_folder/bin/activate  # Activar ambiente virtual
```

Instalación de Django
```sh
$ git clone [git-repo-url] agenda
$ mkdir agenda/tmp  # se creará directorio temporal, aquí irá el ambiente virtual
$ cd agenda/src  # va a la carpeta source
$ ./manage.py migrate  # Aplica migraciones 
$ ./manage.py runserver 0:8080  # Inicializa servidor
```
**[Comuna18][1]**

[1]:http://www.comuna18.com/
